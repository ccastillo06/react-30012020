import React, { useReducer } from "react";

export const PokemonContext = React.createContext();

const initialState = {
  pokemonList: [],
  isLoading: false
};

// Action types
const ADD_POKEMON = "ADD_POKEMON";

// Actions creator
export function addPokemon(pokemon) {
  return {
    type: "ADD_POKEMON",
    payload: {
      pokemon
    }
  };
}

// Selectors
export const getPokemonById = (state, id) => {
  const { pokemonList } = state;
  return pokemonList.find(({ id: pkId }) => `${pkId}` === `${id}`);
}

export const getPokemonByType = (state, type) => {
  const { pokemonList } = state;
  return pokemonList.filter(({ types }) => types.includes(type));
}

// Reducer
const reducer = (state, action) => {
  const { type, payload } = action;

  switch (type) {
    case ADD_POKEMON:
      return {
        ...state,
        pokemonList: [...state.pokemonList, payload.pokemon]
      };
    default:
      return state;
  }
};

// Provider wrapper
export function PokemonContextProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <PokemonContext.Provider value={[state, dispatch]}>
      {children}
    </PokemonContext.Provider>
  );
}
