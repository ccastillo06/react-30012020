import { useEffect } from "react";

// Suponemos que importamos getPeopleFromApi de un servicio
const getPeopleFromApi = () => [{ id: 1, name: "Juan" }];

export function useFetchPeople(successCb, errorCb) {
  useEffect(() => {
    async function fetchPeople() {
      try {
        const people = await getPeopleFromApi();
        successCb(people);
      } catch (e) {
        errorCb(e.message);
      }
    }

    fetchPeople();
  }, [successCb, errorCb]);
}
