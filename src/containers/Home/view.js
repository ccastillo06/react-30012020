import React, { useState } from "react";
import { Link } from "react-router-dom";

import Modal from "../../components/Modal";

function Home() {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <>
      <h1>Welcome to Upgradex</h1>
      <button type="button" onClick={() => setIsOpen(true)}>
        Open modal
      </button>
      <h3>Fetch all the pokemon you want</h3>
      <ul>
        <li>
          <Link to="/pokemon">Pokemon</Link>
        </li>
        <li>
          <Link to="/test">Test sandbox</Link>
        </li>
      </ul>
      <Modal isOpen={isOpen} onClose={() => setIsOpen(false)}>
        <h1>This is the modal</h1>
      </Modal>
    </>
  );
}

export default Home;
