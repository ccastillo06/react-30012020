import React, { useState } from "react";

// Importamos el hook custom
import { useFetchPeople } from "../hooks/usePeople";

// Suponemos que importamos este componente de su archivo
function Loader() {
  return <h3>Loading...</h3>;
}

function ExampleFetch() {
  const [peopleArr, setPeopleArr] = useState([]);
  const [error, setError] = useState(null);

  useFetchPeople(setPeopleArr, setError);

  if (error) {
    return <h3>{error}</h3>;
  }

  return peopleArr.length ? (
    <ul>
      {peopleArr.map(person => (
        <li key={person.id}>
          <p>{person.name}</p>
        </li>
      ))}
    </ul>
  ) : (
    <Loader />
  );
}

export default ExampleFetch;
