import React, { useRef, useEffect } from "react";
import PropTypes from "prop-types";

function Modal({ children, isOpen, onClose }) {
  const PopoverRef = useRef();

  function handleModalClose(e) {
    const { current } = PopoverRef;

    if (e.target === current) {
      onClose();
    }
  }

  useEffect(() => {
    document.addEventListener("click", handleModalClose);

    return () => {
      document.removeEventListener("click", handleModalClose);
    };
  });

  return isOpen ? (
    <div className="Modal">
      <div ref={PopoverRef} className="Modal__popover"></div>
      <dialog className="Modal__dialog" open>
        <button type="button" onClick={onClose}>
          X
        </button>
        {children}
      </dialog>
    </div>
  ) : null;
}

Modal.propTypes = {
  isOpen: PropTypes.bool,
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func.isRequired
};

Modal.defaultProps = {
  isOpen: false
};

export default Modal;
